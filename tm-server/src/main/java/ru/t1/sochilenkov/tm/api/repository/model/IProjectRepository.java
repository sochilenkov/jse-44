package ru.t1.sochilenkov.tm.api.repository.model;

import ru.t1.sochilenkov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
}
