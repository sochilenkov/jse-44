package ru.t1.sochilenkov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.component.Bootstrap;

public final class ApplicationServer {

    public static void main(@Nullable String... args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}